package termstore;

import java.io.Serializable;

public abstract class TermStore implements Serializable {

    public abstract void addSubject(String value);
    public abstract void addPredicate(String value);
    public abstract void addObject(String value);
    public abstract void addObject(Long value);
    public abstract void addObject(Double value);

    public abstract Integer translateSubject(String string);
    public abstract String translateSubject(Integer id);
    
    public abstract Integer translatePredicate(String string);
    public abstract String translatePredicate(Integer id);
    
    public abstract Integer translateObject(String string);
    public abstract Integer translateObject(Long lon);
    public abstract Integer translateObject(Double doub);
    public abstract String translateObject(Integer id);
}
