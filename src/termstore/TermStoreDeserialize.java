package termstore;

import java.io.ObjectInputStream;

public abstract class TermStoreDeserialize {

    protected TermStore termStore;

    public TermStoreDeserialize(TermStore termStore) {
        this.termStore = termStore;
    }

    public TermStore getTermStore() {
        return termStore;
    }

    public abstract void execute(ObjectInputStream stream);
}
