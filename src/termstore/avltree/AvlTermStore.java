package termstore.avltree;

import java.io.Serializable;
import java.util.HashMap;
import termstore.TermStore;

public class AvlTermStore extends TermStore {

    private AvlTree<String, Integer> objectTreeStrings;
    private HashMap<Integer, String> idToStringObject;
    private AvlTree<Long, Integer> objectTreeLongs;
    private HashMap<Integer, Long> idToLongObject;
    private AvlTree<Double, Integer> objectTreeDoubles;
    private HashMap<Integer, Double> idToDoubleObject;
    private HashMap<Integer, String> idToStringSubject;
    private HashMap<String, Integer> stringToIdSubject;
    private HashMap<Integer, String> idToStringPredicate;
    private HashMap<String, Integer> stringToIdPredicate;
    private Integer IDSubject;
    private Integer IDPredicate;
    private Integer IDObject;

    public AvlTermStore() {
        this.IDSubject = 0;
        this.IDPredicate = 0;
        this.IDObject = 0;

        this.idToStringSubject = new HashMap<>();
        this.stringToIdSubject = new HashMap<>();
        this.idToStringPredicate = new HashMap<>();
        this.stringToIdPredicate = new HashMap<>();

        this.idToStringObject = new HashMap<>();
        this.idToDoubleObject = new HashMap<>();
        this.idToLongObject = new HashMap<>();

        this.objectTreeDoubles = new AvlTree<>();
        this.objectTreeLongs = new AvlTree<>();
        this.objectTreeStrings = new AvlTree<>();


    }

    @Override
    public void addSubject(String value) {
        if (!stringToIdSubject.containsKey(value)) {
            idToStringSubject.put(IDSubject, value);
            stringToIdSubject.put(value, IDSubject++);
        }
    }

    @Override
    public void addPredicate(String value) {
        if (!stringToIdPredicate.containsKey(value)) {
            idToStringPredicate.put(IDPredicate, value);
            stringToIdPredicate.put(value, IDPredicate++);
        }
    }

    @Override
    public void addObject(String value) {
        objectTreeStrings.insert(value, IDObject);
        idToStringObject.put(IDObject++, value);
    }

    @Override
    public void addObject(Long value) {
        objectTreeLongs.insert(value, IDObject);
        idToLongObject.put(IDObject++, value);
    }

    @Override
    public void addObject(Double value) {
        objectTreeDoubles.insert(value, IDObject);
        idToDoubleObject.put(IDObject++, value);
    }

    @Override
    public Integer translateSubject(String string) {
        return stringToIdSubject.get(string);
    }

    @Override
    public String translateSubject(Integer id) {
        return idToStringSubject.get(id);
    }

    @Override
    public Integer translatePredicate(String string) {
        return stringToIdPredicate.get(string);
    }

    @Override
    public String translatePredicate(Integer id) {
        return idToStringPredicate.get(id);
    }

    @Override
    public Integer translateObject(String value) {
        return objectTreeStrings.getValue(value);
    }

    @Override
    public Integer translateObject(Long value) {
        return objectTreeLongs.getValue(value);
    }

    @Override
    public Integer translateObject(Double value) {
        return objectTreeDoubles.getValue(value);
    }

    @Override
    public String translateObject(Integer id) {
        String result1 = idToStringObject.get(id);
        if (result1 != null) {
            return result1;
        }
        Double result2 = idToDoubleObject.get(id);
        if (result2 != null) {
            return result2.toString();
        }
        Long result3 = idToLongObject.get(id);
        if (result3 != null) {
            return result3.toString();
        }
        return null;
    }
}
