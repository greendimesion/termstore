package termstore.avltree;

import java.io.Serializable;

public class AvlTree<T extends Comparable, R> implements Serializable{

    protected AvlNode root;
    protected int maxHeight;
    protected int size;

    public AvlTree() {
        this.root = null;
        this.maxHeight = 0;
        this.size = 0;
    }

    public void reset() {
        root = null;
        maxHeight = 0;
        size = 0;
    }

    public boolean isEmpty() {
        return (root == null);
    }

    public AvlNode getRoot() {
        return root;
    }

    public int getHeight() {
        return maxHeight;
    }

    public int size() {
        return size;
    }

    public boolean insert(T value, R id) {
        root = getValue(value, root, id);
        size++;
        return true;
    }

    private AvlNode getValue(T value, AvlNode actualRoot, R id) {
        AvlNode newNode = new AvlNode<>(value, id);
        if (actualRoot == null) {
            actualRoot = newNode;
        } else if (newNode.compareTo(actualRoot) < 0) {
            actualRoot.setLeft(getValue(value, actualRoot.getLeft(), id));
            actualRoot = balancingRight(actualRoot, newNode);
        } else if (newNode.compareTo(actualRoot) > 0) {
            actualRoot.setRight(getValue(value, actualRoot.getRight(), id));
            actualRoot = balancingLeft(actualRoot, newNode);
        }
        int nodeHeight = max(getNodeHeight(actualRoot.getLeft()), getNodeHeight(actualRoot.getRight())) + 1;
        actualRoot.setHeight(nodeHeight);
        if (nodeHeight > getHeight()) {
            this.maxHeight = nodeHeight;
        }
        return actualRoot;
    }

    protected AvlNode balancingRight(AvlNode actualRoot, AvlNode newNode) {
        if (getNodeHeight(actualRoot.getRight()) - getNodeHeight(actualRoot.getLeft()) == -2) {

            if (newNode.compareTo(actualRoot.getLeft()) < 0) {
                actualRoot = rotateWithLeftChild(actualRoot);
            } else {
                actualRoot = doubleWithRightChild(actualRoot);
            }
        }
        return actualRoot;
    }

    protected AvlNode balancingLeft(AvlNode actualRoot, AvlNode newNode) {
        if (getNodeHeight(actualRoot.getRight()) - getNodeHeight(actualRoot.getLeft()) == 2) {

            if (newNode.compareTo(actualRoot.getRight()) > 0) {
                actualRoot = rotateWithRightChild(actualRoot);
            } else {
                actualRoot = doubleWithLeftChild(actualRoot);
            }
        }
        return actualRoot;
    }

    public AvlNode rotateWithLeftChild(AvlNode rightNode) {

        AvlNode leftNode = rightNode.getLeft();

        rightNode.setLeft(leftNode.getRight());
        leftNode.setRight(rightNode);

        rightNode.setHeight(max(getNodeHeight(rightNode.getLeft()), getNodeHeight(rightNode.getRight())) + 1);
        leftNode.setHeight(max(getNodeHeight(leftNode.getLeft()), rightNode.getHeight()) + 1);

        return leftNode;
    }

    public AvlNode rotateWithRightChild(AvlNode leftNode) {

        AvlNode rightNode = leftNode.getRight();

        leftNode.setRight(rightNode.getLeft());
        rightNode.setLeft(leftNode);

        leftNode.setHeight(max(getNodeHeight(leftNode.getLeft()), getNodeHeight(leftNode.getRight())) + 1);
        rightNode.setHeight(max(getNodeHeight(rightNode.getRight()), leftNode.getHeight()) + 1);

        return rightNode;
    }

    public AvlNode doubleWithRightChild(AvlNode rightNode) {
        rightNode.setLeft(rotateWithRightChild(rightNode.getLeft()));
        return rotateWithLeftChild(rightNode);
    }

    public AvlNode doubleWithLeftChild(AvlNode leftNode) {
        leftNode.setRight(rotateWithLeftChild(leftNode.getRight()));
        return rotateWithRightChild(leftNode);
    }

    public static int max(int value1, int value2) {
        return value1 > value2 ? value1 : value2;
    }

    public static int getNodeHeight(AvlNode node) {
        return node == null ? -1 : node.getHeight();
    }

    public R getValue(T value) {
        AvlNode node = getValue(value, root);
        if (node == null) {
            return null;
        }
        return (R) node.getId();
    }

    private AvlNode getValue(T value, AvlNode actualRoot) {
        if (actualRoot == null) {
            actualRoot = null;
        } else if (value.compareTo(actualRoot.getValue()) < 0) {
            actualRoot = getValue(value, actualRoot.getLeft());
        } else if (value.compareTo(actualRoot.getValue()) > 0) {
            actualRoot = getValue(value, actualRoot.getRight());
        }
        return actualRoot;
    }
}