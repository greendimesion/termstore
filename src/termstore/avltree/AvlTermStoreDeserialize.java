package termstore.avltree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import termstore.TermStoreDeserialize;

public class AvlTermStoreDeserialize extends TermStoreDeserialize {

    public AvlTermStoreDeserialize(AvlTermStore termStore) {
        super(termStore);
    }

    @Override
    public void execute(ObjectInputStream stream) {
        try {
            termStore = (AvlTermStore) stream.readObject();
            stream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AvlTermStoreDeserialize.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(AvlTermStoreDeserialize.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
