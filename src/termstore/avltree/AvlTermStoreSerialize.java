package termstore.avltree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import termstore.TermStoreSerialize;

public class AvlTermStoreSerialize extends TermStoreSerialize {

    public AvlTermStoreSerialize(AvlTermStore termStore) {
        super(termStore);
    }

    @Override
    public void execute(ObjectOutputStream stream) {
        try {
            stream.writeObject(termStore);
            stream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AvlTermStoreSerialize.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AvlTermStoreSerialize.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
