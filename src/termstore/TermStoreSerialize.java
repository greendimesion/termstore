package termstore;

import java.io.ObjectOutputStream;

public abstract class TermStoreSerialize {

    protected TermStore termStore;

    public TermStoreSerialize(TermStore termStore) {
        this.termStore = termStore;
    }

    public TermStore getTermStore() {
        return termStore;
    }

    public abstract void execute(ObjectOutputStream stream);
}
