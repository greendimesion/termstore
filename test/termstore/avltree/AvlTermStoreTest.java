package termstore.avltree;

import static org.junit.Assert.*;
import org.junit.Test;
import termstore.TermStore;

public class AvlTermStoreTest {

    public AvlTermStoreTest() {
    }

    @Test
    public void testAddSubject() {
        TermStore termStore = new AvlTermStore();
        termStore.addSubject("Pepe");
        assertEquals(termStore.translateSubject("Pepe"), 0, 0);
        assertEquals(termStore.translateSubject(0), "Pepe");
        assertNull(termStore.translateSubject("Pedro"));
    }

    @Test
    public void testAddPredicate() {
        TermStore termStore = new AvlTermStore();
        termStore.addPredicate("esta");
        assertEquals(termStore.translatePredicate("esta"), 0, 0);
        assertEquals(termStore.translatePredicate(0), "esta");
        assertNull(termStore.translatePredicate("es"));
    }

    @Test
    public void testAddObject_String() {
        TermStore termStore = new AvlTermStore();
        termStore.addPredicate("Pepe");
        assertEquals(termStore.translatePredicate("Pepe"), 0, 0);
        assertEquals(termStore.translatePredicate(0), "Pepe");
        assertNull(termStore.translatePredicate("Pedro"));
    }

    @Test
    public void testAddObject_Long() {
        TermStore termStore = new AvlTermStore();
        termStore.addObject(Long.MIN_VALUE);
        assertEquals(termStore.translateObject(Long.MIN_VALUE), 0, 0);
        assertEquals(termStore.translateObject(0), Long.toString(Long.MIN_VALUE));
        assertNull(termStore.translateObject(Long.MAX_VALUE));
    }

    @Test
    public void testAddObject_Double() {
        TermStore termStore = new AvlTermStore();
        termStore.addObject(Double.MIN_VALUE);
        assertEquals(termStore.translateObject(Double.MIN_VALUE), 0.0, 0.0);
        assertEquals(termStore.translateObject(0), Double.toString(Double.MIN_VALUE));
        assertNull(termStore.translateObject(Double.MAX_VALUE));
    }
}